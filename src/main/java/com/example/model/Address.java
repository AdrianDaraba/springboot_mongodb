package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Address {

	private String city;
    private String country;
    
    
	public Address(String city, String country) {
		super();
		this.city = city;
		this.country = country;
	}

   
}
