package com.example.repository;

import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.ArraySortedAssert;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.model.Address;
import com.example.model.Hotel;
import com.example.model.Review;

@Component
public class DbSeeder implements CommandLineRunner{

	private HotelRepository hotelRepository;

    public DbSeeder(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
}
	
	@Override
	public void run(String... args) throws Exception {
		Hotel marriot = new Hotel(
				"Marriot",
				130,
				new Address("Paris", "France"),
				Arrays.asList(
						new Review("John", 8, false),
						new Review("MAry", 7, true)
						)
		);		
		
		Hotel ibis = new Hotel(
				"Marriot",
				90,
				new Address("Bucharest", "Romania"),
				Arrays.asList(
						new Review("John", 8, false),
						new Review("MAry", 7, true)
						)
		);		
		
		Hotel sofia = new Hotel(
				"Sofiet",
				100,
				new Address("Rome", "Italy"),
				Arrays.asList(
						new Review("John", 8, false),
						new Review("MAry", 7, true)
						)
		);		
		// drop all hotels
		this.hotelRepository.deleteAll();

		// add our hotels to the database
		List<Hotel> hotels = Arrays.asList(marriot, ibis, sofia);
		this.hotelRepository.save(hotels);
	}
	
}
